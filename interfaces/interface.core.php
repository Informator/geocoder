<?php
/**
 * This basics core interface
 * Author Nikolay Baev (a.k.a. Monster3D)
 */

interface CoreFunctions
{
    /**
     * @param $message string set argument func error message
     * @return void
     */
    function setError($message);

    /**
     * @return string error message
     */
    function getError();

}